def userGitlab = "redX1"
def passwordGitlab = "4Li1uAtWDzCBciPRqUw3"
def nomProjet = "Field_experience"
def repository = "gitlab.com/redX1/field_experience.git"
def doTest = false
def portApp =4492
def registry = "registry.gitlab.com/redx1/field_experience" 
def image = registry+":ver-$BUILD_ID"
def hostToDeploy = "192.168.1.13"
def hostUser = "skynetvir"
def containerName = portApp+"-"+userGitlab+"-"+nomProjet
def sonarQubeAuth = "admin"
def idAws = "app-"+Math.abs(new Random().nextInt() % 600) + 1
pipeline {
    agent {
        label 'master'
    }
    options {
        timeout(time: 1, unit: 'HOURS')
    }
    tools {
        nodejs "node"
    }
    stages{
        stage('clone') {
            steps {
               
               git 'https://'+userGitlab+':'+passwordGitlab+'@'+repository
               script{
                 writeFileInRepository('Dockerfile', '''
                   FROM node:lts-alpine
                   # install simple http server for serving static content
                   RUN npm install -g http-server
                   # make the app folder the current working directory
                   WORKDIR /app
                   # copy both package.json and package-lock.json
                   COPY package*.json ./
                   # install project dependencies
                   RUN npm install
                   # copy project files and folders to the current working directory (i.e. app folder)
                   COPY . .
                   # build app for production with minification
                   RUN npm run generate
                   EXPOSE 8080
                   CMD [ "http-server", "dist" ]
                 ''')
               }
            }
        }
        stage('test') {
            steps {
               script {
                 if(doTest) {
                   sh 'node --version'
                   sh 'npm --version'
                   sh 'npm install'
                   sh 'npm run build'
                   sh 'npm test'   
                 }
               }
            }
        }
        stage('analyse code'){
         steps {
           sh 'set +e'
           sh 'docker start sonarqube || docker run -d --name sonarqube -p 9000:9000 sonarqube:7.9-community'
           sh 'set -e'
           script {
               sh 'sleep 149s'
             def scannerHome = tool 'sonarQubeScanner';
             withSonarQubeEnv('sonarQubeServer') {
               sh """${scannerHome}/bin/sonar-scanner \
               -Dsonar.projectKey="""+nomProjet+""" \
               -Dsonar.projectName="""+nomProjet+""" \
               -Dsonar.host.url=http://\$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sonarqube):9000 \
               -Dsonar.sources=. \
               -Dsonar.login="""+sonarQubeAuth+""" \
               -Dsonar.password="""+sonarQubeAuth+""" \
               -Dsonar.exclusions=vendor/** \
               -Dsonar.sourceEncoding=UTF-8 \
               -Dsonar.projectVersion=ver-$BUILD_ID """
             }
            }
          }
        }
        stage('build image') {
         steps {
           script {
             docker.build(image, '.')
           }
         }
        }
        stage('test image') {
         steps {
           script {
             docker.image(image).withRun("-p "+portApp+":8080 --name test-$BUILD_ID"){ c ->
               sh 'sleep 20s'
               sh '''curl $(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' test-$BUILD_ID):8080'''
             }
           }
         }
        }
        stage('push to registry') {
         steps {
           script {
             sh 'docker login registry.gitlab.com -u '+userGitlab+' -p '+passwordGitlab
             sh 'docker tag '+image+' '+registry+':latest'
             sh 'docker push '+registry+':latest'
             sh 'docker push '+image
           }
          }
        }
        stage('deploy - config ansible') {
         steps {
           git 'https://gitlab.com/redX1/deploy_imgdocker_to_ec2.git'
           sh 'chmod 0000 ./ssh-key/ssh_to_ec2'
         }
        }
        stage('deploy - to serveur') {
         steps {
           ansiblePlaybook(
                   playbook: 'createInstance.yml',
                   extras: """--tags create_ec2 --extra-vars 'id="""+idAws+"""'"""
           )
           ansiblePlaybook(
                   playbook: 'deployApp.yml',
                   inventory: '00_inventory.yml',
                   extras: """--tags create_ec2 --extra-vars 'image="""+image+"""
                                             port_container="""+portApp+"""
                                             url_registry=registry.gitlab.com
                                             userGitlab="""+userGitlab+""" 
                                             tokenOrPass="""+passwordGitlab+"""
                                             nom_container="""+containerName+"""'"""
           )
           script {
             sh 'echo Consulter votre application web : http://192.168.1.42:'+portApp
           }
         }
        }
  }
        post {
            success {
               script {
                 sh 'echo Consulter votre application web : http://192.168.1.42:'+portApp
               }
                 
            }
        }
}
private void writeFileInRepository(String fichier, String content){  
   def data = content  
   writeFile(file: fichier, text: data)  
} 
